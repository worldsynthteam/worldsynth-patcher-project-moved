package com.booleanbyte.worldsynth.patcher.ui.fx;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class Infobar extends BorderPane {
	
	private final Label infoLabel;
	
	public Infobar() {
		infoLabel = new Label("Welcome to WorldSynth - Version " + WorldSynthPatcher.VERSION);
		setCenter(infoLabel);
	}
	
	public void updateInfoText(String text) {
		infoLabel.setText(text);
	}
}
