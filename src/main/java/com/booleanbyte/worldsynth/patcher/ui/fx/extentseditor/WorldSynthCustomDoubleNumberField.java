package com.booleanbyte.worldsynth.patcher.ui.fx.extentseditor;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class WorldSynthCustomDoubleNumberField extends Pane {
	
	private final SimpleDoubleProperty value;
	private final TextField numberField;
	
	public WorldSynthCustomDoubleNumberField(double initValue) {
		value = new SimpleDoubleProperty(initValue);
		numberField = new TextField(String.valueOf(initValue));
		getChildren().add(numberField);
		
		numberField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
			try {
				String valueString = newValue.replaceAll(",", ".");
				Double.parseDouble(valueString);
				numberField.setStyle(null);
			} catch (NumberFormatException  ex) {
				numberField.setStyle("-fx-background-color: RED;");
			}
		});
		
		numberField.setOnAction(e -> {
			try {
				String valueString = numberField.getText().replaceAll(",", ".");
				value.set(Double.parseDouble(valueString));
			} catch (NumberFormatException ex) {
				numberField.setText(String.valueOf(value.get()));
			}
			numberField.positionCaret(numberField.getText().length());
		});
		
		value.addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			numberField.setText(String.valueOf(newValue));
		});
	}
	
	public SimpleDoubleProperty valueProperty() {
		return value;
	}
	
	public double getValue() {
		return value.get();
	}
	
	public void setValue(double value) {
		this.value.set(value);
		if(Double.isNaN(value)) {
			numberField.setText("");
		}
		else {
			numberField.setText(String.valueOf(value));
		}
	}
}
