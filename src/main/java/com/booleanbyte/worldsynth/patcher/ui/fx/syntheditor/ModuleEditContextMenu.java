package com.booleanbyte.worldsynth.patcher.ui.fx.syntheditor;

import com.booleanbyte.worldsynth.module.ModuleMacro;
import com.booleanbyte.worldsynth.modulewrapper.ModuleWrapper;
import com.booleanbyte.worldsynth.patcher.ui.fx.WorldSynthEditorController;
import com.booleanbyte.worldsynth.standalone.ui.stage.DeviceNameEditorStage;
import com.booleanbyte.worldsynth.standalone.ui.stage.ModuleParametersStage;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

public class ModuleEditContextMenu extends ContextMenu {
	
	public ModuleEditContextMenu(ModuleWrapper device, SynthEditorPane synthEditor) {
		
		synthEditor.setOnMousePressed(e -> {
			hide();
		});
		
		//PARAMETERS//
		MenuItem openModuleUI = new MenuItem("Parameters");
		openModuleUI.setOnAction(e -> {
			new ModuleParametersStage(device);
		});
		getItems().add(openModuleUI);
		
		//RENAME//
		MenuItem openDeviceCustomName = new MenuItem("Rename");
		openDeviceCustomName.setOnAction(e -> {
			new DeviceNameEditorStage(device, synthEditor);
		});
		getItems().add(openDeviceCustomName);
		
		//BYPASS//
		MenuItem bypasDevice = new MenuItem("Bypass");
		bypasDevice.setDisable(!device.isBypassable());
		bypasDevice.setOnAction(e -> {
			device.setBypassed(!device.isBypassed());
		});
		getItems().add(bypasDevice);
		
		//DELETE//
		MenuItem deleteDevice = new MenuItem("Delete");
		deleteDevice.setOnAction(e -> {
			synthEditor.removeDevice(device, true, true);
		});
		getItems().add(deleteDevice);
		
		if(device.module instanceof ModuleMacro) {
			MenuItem openMacro = new MenuItem("OpenMacro");
			openMacro.setOnAction(e -> {
				WorldSynthEditorController.instance.openSynthEditor(((ModuleMacro)device.module).getMacroSynth(), null); 
			});
			getItems().add(openMacro);
		}
	}
}
