package com.booleanbyte.worldsynth.patcher.ui.fx.syntheditor.actioncontroll;

public enum InputState {
	YES(true),
	NO(false),
	UNUSED(false);
	
	private boolean state;
	
	InputState(boolean state) {
		this.state = state;
	}
	
	public boolean getState() {
		return state;
	}
}
