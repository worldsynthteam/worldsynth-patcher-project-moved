package com.booleanbyte.worldsynth.patcher.ui.fx.syntheditor.actioncontroll;

public interface KeyboardAction<T> {
	
	public EditorKeyboardInputFilter[] getActionFilters();
	public void action(T t);
}
