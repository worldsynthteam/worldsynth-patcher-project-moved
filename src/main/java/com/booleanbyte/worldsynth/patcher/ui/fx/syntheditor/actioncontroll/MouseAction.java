package com.booleanbyte.worldsynth.patcher.ui.fx.syntheditor.actioncontroll;

public interface MouseAction<T> {
	
	public EditorMouseInputFilter getActionFilter();
	public void action(T t);
}
