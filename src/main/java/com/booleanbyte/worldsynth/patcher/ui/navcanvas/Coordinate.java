package com.booleanbyte.worldsynth.patcher.ui.navcanvas;

public class Coordinate {
	
	public double x;
	public double y;
	
	public Coordinate(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Coordinate(Pixel p, NavigationalCanvas navCanvas) {
		double x = p.x;
		double y = p.y;
		
		x -= navCanvas.getWidth() / 2.0;
		y -= navCanvas.getHeight() / 2.0;
		
		x += navCanvas.getCenterCoordinateX() * navCanvas.getZoom();
		y += navCanvas.getCenterCoordinateY() * navCanvas.getZoom();
		
		x /= navCanvas.getZoom();
		y /= navCanvas.getZoom();
		
		this.x = x;
		this.y = y;
	}
}