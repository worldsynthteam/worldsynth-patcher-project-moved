package com.booleanbyte.worldsynth.patcher.ui.navcanvas;

public interface NavigationalCanvas {
	
	public double getCenterCoordinateX();
	public double getCenterCoordinateY();
	public double getZoom();
	
	public double getWidth();
	public double getHeight();
}
