package com.booleanbyte.worldsynth.patcher.ui.navcanvas;

public class Pixel {
	
	public double x;
	public double y;
	
	public Pixel(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Pixel(Coordinate c, NavigationalCanvas navCanvas) {
		double x = c.x * navCanvas.getZoom();
		double y = c.y * navCanvas.getZoom();
		
		x -= navCanvas.getCenterCoordinateX() * navCanvas.getZoom();
		y -= navCanvas.getCenterCoordinateY() * navCanvas.getZoom();
		
		x += navCanvas.getWidth() / 2.0;
		y += navCanvas.getHeight() / 2.0;
		
		this.x = x;
		this.y = y;
	}
}