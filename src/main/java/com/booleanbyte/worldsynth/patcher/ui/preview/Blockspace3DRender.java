package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeBlockspace;
import com.booleanbyte.worldsynth.glpreview.fx.Blockspace3dViewer;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class Blockspace3DRender extends AbstractPreviewRender {
	
	private final Blockspace3dViewer blockspaceViewer;
	
	public Blockspace3DRender() {
		super();
		blockspaceViewer = new Blockspace3dViewer();
		blockspaceViewer.setPrefSize(450, 450);
		getChildren().add(blockspaceViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        blockspaceViewer.setLayoutX(x);
        blockspaceViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeBlockspace castData = (DatatypeBlockspace) data;
		blockspaceViewer.setBlockspace(castData.blockspaceMaterialId);
	}
}
