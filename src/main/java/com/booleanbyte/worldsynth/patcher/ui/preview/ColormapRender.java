package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeColormap;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ColormapRender extends AbstractPreviewRenderCanvas {
	
	private float[][][] colormap;
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeColormap castData = (DatatypeColormap) data;
		this.colormap = castData.colorMap;
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(colormap != null) {
			
			double xOffset = (getWidth() - colormap.length)/2;
			double yOffset = (getHeight() - colormap[0].length)/2;
			
			for(int x = 0; x < colormap.length; x++) {
				for(int y = 0; y < colormap[x].length; y++) {
					g.setFill(Color.color(colormap[x][y][0], colormap[x][y][1], colormap[x][y][2]));
					g.fillRect(x + xOffset, y + yOffset, 1, 1);
				}
			}
		}
	}
}
