package com.booleanbyte.worldsynth.patcher.ui.preview;


import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeFeaturemap;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class FeaturemapRender extends AbstractPreviewRenderCanvas {

	private double[][] points;
	
	public double x, z, width, length;
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeFeaturemap castData = (DatatypeFeaturemap) data;
		this.points = castData.points;
		this.x = castData.x;
		this.z = castData.z;
		this.width = castData.width;
		this.length = castData.length;
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(points != null) {
			double resPitch = 250.0 / Math.max(width, length);
			
			double xOffset = getWidth()/2 - (width*resPitch)/2.0;
			double yOffset = getHeight()/2 - (length*resPitch)/2.0;
			
			g.setFill(Color.BLACK);
			g.fillRect(xOffset, yOffset, width*resPitch, length*resPitch);
			
			for(double[] p: points) {
				g.setFill(Color.WHITE);
				g.fillRect((p[0]-x) * resPitch + xOffset - 1, (p[1]-z) * resPitch + yOffset - 1, 3, 3);
			}
		}
	}
}
