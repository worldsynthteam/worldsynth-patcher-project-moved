package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeFeaturespace;
import com.booleanbyte.worldsynth.glpreview.fx.Featurespace3dViewer;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class Featurespace3DRender extends AbstractPreviewRender {
	
	private final Featurespace3dViewer featurespaceViewer;
	
	public Featurespace3DRender() {
		super();
		featurespaceViewer = new Featurespace3dViewer();
		featurespaceViewer.setPrefSize(450, 450);
		getChildren().add(featurespaceViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        featurespaceViewer.setLayoutX(x);
        featurespaceViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeFeaturespace castData = (DatatypeFeaturespace) data;
		featurespaceViewer.setFeaturespace(castData.points, castData.x, castData.y, castData.z, castData.width, castData.height, castData.length);
	}
}
