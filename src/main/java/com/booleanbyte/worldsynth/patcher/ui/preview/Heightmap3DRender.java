package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.glpreview.fx.Heightmap3dViewer;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class Heightmap3DRender extends AbstractPreviewRender {
	
	private final Heightmap3dViewer heightmapViewer;
	
	public Heightmap3DRender() {
		super();
		heightmapViewer = new Heightmap3dViewer();
		heightmapViewer.setPrefSize(450, 450);
		getChildren().add(heightmapViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        heightmapViewer.setLayoutX(x);
        heightmapViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeHeightmap castData = (DatatypeHeightmap) data;
		heightmapViewer.setHeightmap(castData.getHeightmap(), 255, Math.max(castData.width, castData.length));
	}
}
