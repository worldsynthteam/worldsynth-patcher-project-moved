package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmapOverlay;
import com.booleanbyte.worldsynth.glpreview.fx.Overlay3dViewer;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class HeightmapOverlay3DRender extends AbstractPreviewRender {
	
	private final Overlay3dViewer overlayViewer;
	
	public HeightmapOverlay3DRender() {
		super();
		overlayViewer = new Overlay3dViewer();
		overlayViewer.setPrefSize(450, 450);
		getChildren().add(overlayViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        overlayViewer.setLayoutX(x);
        overlayViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeHeightmapOverlay castData = (DatatypeHeightmapOverlay) data;
		overlayViewer.setHeightmap(castData.heightMap, castData.colorMap, 255, Math.max(castData.width, castData.length));
	}
}
