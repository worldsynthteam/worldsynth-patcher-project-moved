package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeMaterialmap;
import com.booleanbyte.worldsynth.material.MaterialRegistry;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class MaterialmapRender extends AbstractPreviewRenderCanvas {
	
	private int[][] materialmap;
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeMaterialmap castData = (DatatypeMaterialmap) data;
		this.materialmap = castData.materialMap;
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(materialmap != null) {
			
			double xOffset = (getWidth() - materialmap.length)/2;
			double yOffset = (getHeight() - materialmap[0].length)/2;
			
			for(int x = 0; x < materialmap.length; x++) {
				for(int y = 0; y < materialmap[x].length; y++) {
					g.setFill(materialToColor(materialmap[x][y]));
					g.fillRect(x + xOffset, y + yOffset, 1, 1);
				}
			}
		}
	}
	
	private Color materialToColor(int material) {
		return MaterialRegistry.REGISTER.get(material).getFxColor();
	}
}
