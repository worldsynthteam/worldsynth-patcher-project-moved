package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class NullRender extends AbstractPreviewRenderCanvas {
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.setFill(Color.WHITE);
		g.setFont(new Font("TimesRoman", 40));
		g.setTextAlign(TextAlignment.CENTER);
		g.fillText("NULL", getWidth()/2, getHeight()/2-40);
		g.fillText("NO DATA TO RENDER", getWidth()/2, getHeight()/2+40);
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
	}
}
