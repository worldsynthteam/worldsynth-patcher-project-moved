package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.customobject.CustomObject;
import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeObjects;
import com.booleanbyte.worldsynth.glpreview.fx.Object3dViewer;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class Objects3DRender extends AbstractPreviewRender {
	
	private final Object3dViewer objectViewer;
	
	public Objects3DRender() {
		super();
		objectViewer = new Object3dViewer();
		objectViewer.setPrefSize(450, 450);
		getChildren().add(objectViewer);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        objectViewer.setLayoutX(x);
        objectViewer.setLayoutY(y);
    }
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeObjects castData = (DatatypeObjects) data;
		if(castData.objects.length > 0) {
			objectViewer.setObject(castData.objects[0].getObject());
		}
		else {
			objectViewer.setObject(new CustomObject(new int[0][4]));
		}
	}
}
