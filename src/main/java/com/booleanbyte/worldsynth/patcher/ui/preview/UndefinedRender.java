package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class UndefinedRender extends AbstractPreviewRenderCanvas {

	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.2));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.setFill(Color.WHITE);
		g.setFont(new Font("TimesRoman", 40));
		g.fillText("UNDEFINED RENDER", 40, getHeight()/2-40);
		
		g.fillText("MISSING RENDER", 60, getHeight()/2+40);
		g.fillText("FOR THIS DATATYPE", 40, getHeight()/2+80);
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
	}
}
