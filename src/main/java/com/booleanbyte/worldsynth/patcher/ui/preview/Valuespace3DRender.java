package com.booleanbyte.worldsynth.patcher.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeValuespace;
import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Valuespace3DRender extends AbstractPreviewRenderCanvas {
	
	@SuppressWarnings("unused")
	private float[][][] valuespace;
	
	public Valuespace3DRender() {
		super();
	}
	
	public void setVoxelValuespace(float[][][] valuespace) {
		this.valuespace = valuespace;
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data) {
		DatatypeValuespace castData = (DatatypeValuespace) data;
		setVoxelValuespace(castData.valuespace);
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.RED);
		g.fillRect(0, 0, getWidth(), getHeight());
	}
}
