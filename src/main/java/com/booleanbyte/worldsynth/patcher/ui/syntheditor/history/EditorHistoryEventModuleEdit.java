package com.booleanbyte.worldsynth.patcher.ui.syntheditor.history;

import com.booleanbyte.worldsynth.modulewrapper.ModuleWrapper;
import com.booleanbyte.worldsynth.synth.io.Element;

public class EditorHistoryEventModuleEdit extends AbstractEditorHistoryEvent {
	
	private ModuleWrapper subject;
	private Element oldParameterElemetn;
	private Element newParameterElement;
	
	public EditorHistoryEventModuleEdit(ModuleWrapper subjects, Element oldParameterElement, Element newParameterElement) {
		this.subject = subjects;
		this.oldParameterElemetn = oldParameterElement;
		this.newParameterElement = newParameterElement;
	}
	
	public ModuleWrapper getSubject() {
		return subject;
	}
	
	public Element getOldParameterElement() {
		return oldParameterElemetn;
	}
	
	public Element getNewParameterElement() {
		return newParameterElement;
	}
}
