package com.booleanbyte.worldsynth.patcher.ui.syntheditor.history;

import com.booleanbyte.worldsynth.patcher.ui.navcanvas.Coordinate;

public class EditorHistoryEventModuleMove extends AbstractEditorHistoryEvent {
	
	private Object[] moveSubjects;
	private Coordinate[] oldCoordinates;
	private Coordinate[] newCoordinates;
	
	public EditorHistoryEventModuleMove(Object[] moveSubjects, Coordinate[] oldCoordinates, Coordinate[] newCoordinates) {
		this.moveSubjects = moveSubjects;
		this.oldCoordinates = oldCoordinates;
		this.newCoordinates = newCoordinates;
	}
	
	public Object[] getMoveSubjects() {
		return moveSubjects;
	}
	
	public Coordinate[] getOldCoordinates() {
		return oldCoordinates;
	}
	
	public Coordinate[] getNewCoordinates() {
		return newCoordinates;
	}
	
	public void setOldCoordinates(Coordinate[] oldCoordinates) {
		this.oldCoordinates = oldCoordinates;
	}
	
	public void setNewCoordinates(Coordinate[] newCoordinates) {
		this.newCoordinates = newCoordinates;
	}
}
