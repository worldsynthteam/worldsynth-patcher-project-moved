package com.booleanbyte.worldsynth.standalone.ui.stage;

public class PinnableUtilityStage extends UtilityStage {
	
	private boolean pinned = false;
	
	public PinnableUtilityStage() {}
	
	public void setPinned(boolean pin) {
		pinned = pin;
	}
	
	public boolean isPinned() {
		return pinned;
	}
}
